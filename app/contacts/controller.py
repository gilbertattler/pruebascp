from flask import Blueprint, request, jsonify, make_response
from flask_restful import Api, Resource, reqparse, fields, marshal_with
from app.database import db
from app.contacts.models import Contacts, ContactsSchema
from sqlalchemy.exc import SQLAlchemyError
from marshmallow import ValidationError

contacts = Blueprint('contacts', __name__)
 
schema = ContactsSchema(strict=True)

api = Api(contacts)

class CreateListContacts(Resource):
    
    def get(self):
        contacts_query = Contacts.query.all()
        results = schema.dump(contacts_query, many=True).data
        return results
 
    def post(self):
        raw_dict = request.get_json(force=True)
        try:
            schema.validate(raw_dict)
            request_dict = raw_dict['data']['attributes']
            contact = Contacts(request_dict['name'], request_dict['address'], request_dict['country'], request_dict['city'],request_dict['state'],request_dict['notes'],request_dict['phone'],request_dict['cellphone'],request_dict['user_id'])
            contact.add(contact)
            query = Contacts.query.get(contact.id)
            results = schema.dump(query).data
            return results, 201
 
        except ValidationError as err:
            resp = jsonify({"error": err.messages})
            resp.status_code = 403
            return resp
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 403
            return resp
 
 
class GetUpdateDeleteContact(Resource):
    
    def get(self, id):
        contact_query = Contacts.query.get_or_404(id)
        result = schema.dump(contact_query).data
        return result
 
    def patch(self, id):
        contact = Contacts.query.get_or_404(id)
        raw_dict = request.get_json(force=True)
        try:
            schema.validate(raw_dict)
            request_dict = raw_dict['data']['attributes']
            for key, value in request_dict.items():
                setattr(contact, key, value)
 
            contact.update()
            return self.get(id)
 
        except ValidationError as err:
            resp = jsonify({"error": err.messages})
            resp.status_code = 401
            return resp
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 401
            return resp
 
    def delete(self, id):
        contact = Contacts.query.get_or_404(id)
        try:
            delete = contact.delete(contact)
            response = make_response()
            response.status_code = 204
            return response
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 401
            return resp

#Mapear las clases para los endpoints de la API
api.add_resource(CreateListContacts, '.json')
api.add_resource(GetUpdateDeleteContact, '/<int:id>.json')
