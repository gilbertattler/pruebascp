from flask import Blueprint, request, jsonify, make_response
from flask_restful import Api, Resource, reqparse, fields, marshal_with
from app.database import db
from app.models import Users, UsersSchema, PasswordReset, PasswordResetSchema,bcrypt,redis
from sqlalchemy.exc import SQLAlchemyError
from marshmallow import ValidationError

from app.mixins import SignupLoginMixin

from app.utils.auth import auth_required, admin_required, generate_token
from app.utils.errors import EMAIL_IN_USE, CODE_NOT_VALID, BAD_CREDENTIALS
#Initialize a Flask Blueprint,
users = Blueprint('users', __name__)
passwordreset = Blueprint('passwordreset', __name__)
 
#Initialize the UserSchema we defined in models.py
schema = UsersSchema(strict=True)
schema = PasswordResetSchema(strict=True)
 
#Initialize the  API  object using the Flask-RESTful API class
api = Api(users)
 
user_fields = {
    'id': fields.Integer,
    'email': fields.String
}
# Create CRUD classes using the Flask-RESTful Resource class
class CreateListUsers(Resource):
    
    def get(self):
        users_query = Users.query.all()
        results = schema.dump(users_query, many=True).data
        return results
 
    def post(self):
        raw_dict = request.get_json(force=True)
        try:
            schema.validate(raw_dict)
            request_dict = raw_dict['data']['attributes']
            print(raw_dict)
            user = Users(request_dict['firstname'], request_dict['lastname'], request_dict['email'], request_dict[
                         'passwords'])
            user.add(user)
            return {
                'id': user.id,
                'token': generate_token(user)
            }, 201
 
        except ValidationError as err:
            resp = jsonify({"error": err.messages})
            resp.status_code = 403
            return resp
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 403
            return resp
 
 
class GetUpdateDeleteUser(Resource):
    
    def get(self, id):
        user_query = Users.query.get_or_404(id)
        result = schema.dump(user_query).data
        return result
 
    def patch(self, id):
        user = Users.query.get_or_404(id)
        raw_dict = request.get_json(force=True)
        try:
            schema.validate(raw_dict)
            request_dict = raw_dict['data']['attributes']
            for key, value in request_dict.items():
                setattr(user, key, value)
 
            user.update()
            return self.get(id)
 
        except ValidationError as err:
            resp = jsonify({"error": err.messages})
            resp.status_code = 401
            return resp
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 401
            return resp
 
    def delete(self, id):
        user = Users.query.get_or_404(id)
        try:
            delete = user.delete(user)
            response = make_response()
            response.status_code = 204
            return response
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 401
            return resp
class UserAPI(SignupLoginMixin, restful.Resource):

    @auth_required
    @marshal_with(user_fields)
    def get(self):
        return g.current_user

    def post(self):
        args = self.req_parser.parse_args()

        user = Users(email=args['email'], password=args['password'])
        db.session.add(user)

        try:
            db.session.commit()
        except IntegrityError:
            return EMAIL_IN_USE

        return {
            'id': user.id,
            'token': generate_token(user)
        }, 201


class AuthenticationAPI(SignupLoginMixin, restful.Resource):

    def post(self):
        args = self.req_parser.parse_args()

        user = db.session.query(AppUser).filter(AppUser.email==args['email']).first()
        if user and bcrypt.check_password_hash(user.password, args['password']):

            return {
                'id': user.id,
                'token': generate_token(user)
            }

        return BAD_CREDENTIALS


class PasswordResetRequestAPI(restful.Resource):

    def post(self):
        req_parser = reqparse.RequestParser()
        req_parser.add_argument('email', type=str, required=True)
        args = req_parser.parse_args()

        user = db.session.query(AppUser).filter(AppUser.email==args['email']).first()
        if user:
            password_reset = PasswordReset(user=user)
            db.session.add(password_reset)
            db.session.commit()
            # TODO: Send the email using any preferred method

        return {}, 201


class PasswordResetConfirmAPI(restful.Resource):

    def post(self):
        req_parser = reqparse.RequestParser()
        req_parser.add_argument('code', type=str, required=True)
        req_parser.add_argument('password', type=str, required=True)
        args = req_parser.parse_args()

        password_reset = db.session.query(PasswordReset
                            ).filter(PasswordReset.code==args['code']
                            ).filter(PasswordReset.date>datetime.now()).first()

        if not password_reset:
            return CODE_NOT_VALID

        password_reset.user.set_password(args['password'])
        db.session.delete(password_reset)
        db.session.commit()

        return {}, 200



class AdminOnlyAPI(restful.Resource):

    @admin_required
    def get(self):
        return {}, 200

#Map classes  to API enpoints
api.add_resource(CreateListUsers, '')
#api.add_resource(GetUpdateDeleteUser, '/<int:id>')
api.add_resource(Signin, '/signin/<int:username>/<>')

rest_api.add_resource(users_api.UserAPI, '/api/v1/user')
rest_api.add_resource(users_api.AuthenticationAPI, '/api/v1/authenticate')
rest_api.add_resource(users_api.PasswordResetRequestAPI, '/api/v1/password-reset/request')
rest_api.add_resource(users_api.PasswordResetConfirmAPI, '/api/v1/password-reset/confirm')
rest_api.add_resource(users_api.AdminOnlyAPI, '/api/v1/admin')
