from marshmallow_jsonapi import Schema, fields
from marshmallow import validate
from werkzeug import generate_password_hash, check_password_hash

from app.database import db
 
class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(100), nullable=True)
    lastname = db.Column(db.String(100), nullable=True)
    email = db.Column(db.String(120),nullable = False, unique=True)
    passhash = db.Column(db.String(256),nullable = False)
 
    def __init__(self,  firstname,  lastname,  email,  password):
        self.firstname = firstname
        self.lastname = lastname
        self.email = email.lower()
        self.set_password(password)
         
    def add(self, resource):
        db.session.add(resource)
        return db.session.commit()
 
    def update(self):
        return db.session.commit()
 
    def delete(self, resource):
        db.session.delete(resource)
        return db.session.commit()
     
    def set_password(self, password):
        self.passhash = generate_password_hash(password)
   
    def check_password(self, password):
        return check_password_hash(self.passhash, password)


class UsersSchema(Schema):
 
    not_blank = validate.Length(min=1, error='Los campos no deben estar en blanco')

    id = fields.Integer(dump_only=True)   
    firstname = fields.String(validate=not_blank)
    lastname = fields.String(validate=not_blank)
    email = fields.String(required=True)
    passhash = fields.String(validate=not_blank)
 
    # self links
    def get_top_level_links(self, data, many):
        if many:
            self_link = "/users/"
        else:
            self_link = "/users/{}".format(data['id'])
        return {'self': self_link}
    class Meta:
        type_ = 'users'