from flask import Flask
from app.database import db

def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_object(config_filename)
    db.init_app(app)

    from flask import render_template, send_from_directory    
    
    from app.users.controller import users
    app.register_blueprint(users, url_prefix='/api/v1/users')

    from app.contacts.controller import contacts
    app.register_blueprint(contacts, url_prefix='/api/v1/contacts')
    
    return app