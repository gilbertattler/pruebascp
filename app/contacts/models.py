from marshmallow_jsonapi import Schema, fields
from marshmallow import validate
from flask_sqlalchemy import SQLAlchemy

from app.database import db
 
class Contacts(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), nullable=False)
    address = db.Column(db.String(255), nullable=False)
    country = db.Column(db.String(100), nullable=False)
    city = db.Column(db.String(100), nullable=False)
    state = db.Column(db.String(100), nullable=False)
    notes = db.Column(db.String(255), nullable=True)
    phone = db.Column(db.String(20), nullable=False)
    cellphone = db.Column(db.String(20), nullable=True)
    user_id = db.Column(db.Integer, nullable=False)
    #user_id = db.Column(db.Integer, db.ForeignKey('Users.id'))
    #user = db.relationship(Users)
 
    def __init__(self,  name,  address,  country,  city, state, notes, phone, cellphone, user_id):
        self.name = name
        self.address = address
        self.country = country
        self.city = city
        self.state = state
        self.notes = notes
        self.phone = phone
        self.cellphone = cellphone
        self.user_id = user_id
         
    def add(self, resource):
        db.session.add(resource)
        return db.session.commit()
 
    def update(self):
        return db.session.commit()
 
    def delete(self, resource):
        db.session.delete(resource)
        return db.session.commit()

class ContactsSchema(Schema):
 
    not_blank = validate.Length(min=1, error='Los campos no deben estar en blanco')

    id = fields.Integer(dump_only=True) 
    name = fields.String(validate=not_blank)
    address = fields.String(validate=not_blank)
    country = fields.String(validate=not_blank)
    city = fields.String(validate=not_blank)
    state = fields.String(validate=not_blank)
    notes = fields.String(validate=not_blank)
    phone = fields.String(validate=not_blank)
    cellphone = fields.String(validate=not_blank)
    user_id = fields.Integer(required=True) 
    #user = fields.Nested(UserSchema)
 
    # self links
    def get_top_level_links(self, data, many):
        if many:
            self_link = "/contacts/"
        else:
            self_link = "/contacts/{}".format(data['id'])
        return {'self': self_link}
    class Meta:
        type_ = 'contacts'