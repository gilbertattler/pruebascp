from marshmallow_jsonapi import Schema, fields
from marshmallow import validate
from flask_sqlalchemy import SQLAlchemy

from datetime import datetime, timedelta
from app.utils.misc import make_code

from flask_bcrypt import Bcrypt
from flask_redis import FlaskRedis 

db = SQLAlchemy()
bcrypt = Bcrypt()
redis = FlaskRedis(app)

def expiration_date():
    return datetime.now() + timedelta(days=1)
 

class PasswordReset(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('Users.id'))
    code = db.Column(db.String(255), unique=True, default=make_code)
    date = db.Column(db.DateTime(), default=expiration_date)

    user = db.relationship(Users)

    db.UniqueConstraint('user_id', 'code', name='uni_user_code')

    def __init__(self, user):
        self.user = user

class PasswordResetSchema(Schema):
 
    not_blank = validate.Length(min=1, error='Los campos no deben estar en blanco')
    # add validate=not_blank in required fields
    id = fields.Integer(dump_only=True)   
    user_id = fields.String(validate=not_blank)
    code = fields.String(validate=not_blank)
    date = fields.String(required=True)
 
    # self links
    def get_top_level_links(self, data, many):
        self_link = "/PasswordReset/{}".format(data['id'])
        return {'self': self_link}
            #The below type object is a resource identifier object as per               http://jsonapi.org/format/#document-resource-identifier-objects
    class Meta:
        type_ = 'passwordreset'